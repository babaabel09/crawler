"use client";

import { useState } from "react";
import axios from "axios";
import { callProducts } from "../lib/db";

export default function Home() {
  const [url, setUrl] = useState("");
  const [pageTitle, setPageTitle] = useState("");
  const [dbResult, setDbResult] = useState([]);

  const [loading, setLoading] = useState(false);
  const [loadingDb, setLoadingDb] = useState(false);

  const handleSubmit = async () => {
    setLoading(true);
    try {
      const response = await axios.post("/api/fetch-html", { url });

      console.log(response.data);
      const html = response.data;
      console.log("html", html);
      const parser = new DOMParser();
      const doc = parser.parseFromString(html, "text/html");
      const title = doc.querySelector("title");
      const pageTitle = title ? title.textContent : "No title found";
      setPageTitle(pageTitle);
      setLoading(false);
    } catch (error) {
      setPageTitle("Failed to get result");
      setLoading(false);
      console.error("Error fetching page title", error);
    }
  };

  // QUERY FOR DB
  const fetchDataFromDatabase = async () => {
    setLoadingDb(true);

    try {
      const response = await axios.get("/api/fetch-html");
      setDbResult(response.data);
      setLoadingDb(false);

      console.log(response.data);
    } catch (error) {
      console.error("Error", error);
      setLoadingDb(false);
    }
  };

  // Call the fetchDataFromDatabase function to initiate the database operation

  return (
    <div
      className="
    flex
    jutify-center
    items-center
    flex-col
    w-full
    h-[100vh]

    "
    >
      <div className="my-auto">
        <div
          className="
         flex
         flex-col
         items-center
         "
        >
          <input
            className="
        w-[320px] py-[12px] outline-none px-[20px] my-[8px ] inline-block border border-solid text-black border-gray-300 rounded-4px box-border
        "
            type="text"
            value={url}
            onChange={(e) => setUrl(e.target.value)}
            placeholder="Enter URL"
          />

          <button
            className="
          
            mt-5
            bg-[#bae2bb]
             text-white
             w-[150px]
             py-[14px]
             px-[20px]
             border-0
             rounded-sm
             cursor-pointer;
         "
            onClick={handleSubmit}
          >
            Get Page Title
          </button>
        </div>
        <>
          {loading ? (
            <div
              className="
          mt-10
          "
            >
              Title - Loading...
            </div>
          ) : (
            <div
              className="
                mt-10
                "
            >
              Title - {pageTitle}
            </div>
          )}
        </>
        <button
          className="
          
            mt-5
            bg-[#bae2bb]
             text-white
             w-[150px]
             py-[14px]
             px-[20px]
             border-0
             rounded-sm
             cursor-pointer;
         "
          onClick={fetchDataFromDatabase}
        >
          Get result
        </button>

        <>
          <h5 className="my-5 font-bold">Results:</h5>

          {loadingDb ? (
            <p>Loading...</p>
          ) : (
            <>
              {dbResult &&
                dbResult.map((data, ind) => {
                  return (
                    <>
                      <div key={ind}>
                        <p>
                          Extra:{" "}
                          <span className="text-[green]">{data.Extra}</span>
                        </p>
                        <p>
                          filtered:{" "}
                          <span className="text-[green]">{data.filtered}</span>
                        </p>
                        <p>
                          id: <span className="text-[green]">{data.id}</span>
                        </p>
                        <p>
                          key: <span className="text-[green]">{data.key}</span>
                        </p>
                        <p>
                          key_len:
                          <span className="text-[green]"> {data.key_len}</span>
                        </p>
                        <p>
                          partition:
                          <span className="text-[green]">
                            {data.partitions ? data.partitions : " null"}
                          </span>
                        </p>
                        <p>
                          possible_keys:
                          <span className="text-[green]">
                            {" "}
                            {data.possible_keys}
                          </span>
                        </p>
                        <p>
                          ref:
                          <span className="text-[green]">
                            {data.ref ? data.ref : " null"}
                          </span>
                        </p>
                        <p>
                          rows:
                          <span className="text-[green]"> {data.rows}</span>
                        </p>

                        <p>
                          select_type:
                          <span className="text-[green]">
                            {" "}
                            {data.select_type}
                          </span>
                        </p>
                        <p>
                          table:{" "}
                          <span className="text-[green]">{data.table}</span>
                        </p>
                        <p>
                          type:{" "}
                          <span className="text-[green]"> {data.type}</span>
                        </p>
                      </div>
                    </>
                  );
                })}
            </>
          )}
        </>
      </div>
    </div>
  );
}
