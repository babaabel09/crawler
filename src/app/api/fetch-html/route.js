import { Query } from "@/lib/db";

export async function POST(request) {
  // console.log(request.json());
  const { url } = await request.json();

  try {
    const response = await fetch(url + "?bypass_error_handling=true");
    const html = await response.text();
    return new Response(html, { status: 200 });
  } catch (error) {
    console.error("Error fetching page", error.message);
    return new Response("Failed to fetch HTML content", { status: 500 });
  }
}

export async function GET(request) {
  const getReview = await Query({
    query:
      "explain select * from reviews where type = 'data_audit' and last_submitted_at between '2024-02-01 00:20:31' and '2024-02-15 19:39:21' and priority_level = 'high_priority'and priority_level is not null;",
    data: [],
  });

  let result = JSON.stringify(getReview);
  return new Response(result, {
    status: 200,
  });
}
