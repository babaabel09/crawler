"use server";

import mysql from "mysql2/promise";

export async function Query({ query, data = [] }) {
  try {
    const db = await mysql.createConnection({
      host: "localhost",
      database: "review",
      user: "root",
    });

    const [result] = await db.execute(query, data);
    console.log(`connected on port 3306`);
    await db.end();
    return result;
  } catch (error) {
    console.log(error);
    return error;
  }
}
// callProducts(
//   "explain select * from reviews where type = 'data_audit' and last_submitted_at between '2024-02-01 00:20:31' and '2024-02-15 19:39:21' and priority_level = 'high_priority'and priority_level is not null;"
// );
